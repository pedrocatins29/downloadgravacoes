import React from "react";
import "./custom.scss";

export default function Home() {
  function getLastMonths(n) {
    const month = [
      "Janeiro",
      "Fevereiro",
      "Março",
      "Abril",
      "Maio",
      "Junho",
      "Julho",
      "Agosto",
      "Setembro",
      "Outubro",
      "Novembro",
      "Dezembro"
    ];
    var last_n_months = [];
    var d = new Date();
    for (var i = 0; i < n; i++) {
      last_n_months[i] =
        month[d.getMonth()] + "- " + d.getFullYear().toString();
      d.setMonth(d.getMonth() - 1);
    }
    return last_n_months;
  }
  const month = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
  ];

  return (
    <div className="container col-md">
      <h1 className="container col-12 center d-flex justify-content-center">
        Download de gravações PABX CLOUD LIFE
      </h1>
      <ul>
        {getLastMonths(4)
          .slice(1)
          .map(item => {
            const stringDiferente = item.split("-", 1);
            return (
              <li className="container d-flex justify-content-center">
                <a
                  download
                  href={`/gravacoesMes${month.indexOf(stringDiferente[0]) +
                    1}.zip`}
                >
                  {item}
                </a>
              </li>
            );
          })}
      </ul>
    </div>
  );
}
