import zipfile as zipf
import os

pastas = ["12"]

for i in pastas:
    def zipar(arqs):
        with zipf.ZipFile("gravacoesMes{}.zip".format(i), "w", zipf.ZIP_DEFLATED) as z:
            for arq in arqs:
                if(os.path.isfile(arq)):
                    z.write(arq)
                else:
                    for root, dirs, files in os.walk(arq):
                        for f in files:
                            z.write(os.path.join(root, f))
    print("zipando pasta {}".format(i))
    zipar(["C:/Users/pedro.souza/Desktop/{}".format(i)])
    print("pasta {} zipada".format(i))
